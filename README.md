# QDocumentView

A simple widget to display paged documents, like PDF, DjVu, PS and others.. Currently, PDF, DjVu, PS and EPS are natively supported.

## Features
* Page-rotations: 0, 90, 180, 270
* Page Layout: Single, Facing, Book
* Search highlight (if supported by the backend)
* Custom zoom in the range 10% to 1600%

## Dependencies:
### Main library
* Qt (Qt5, Qt6) - Widgets, PrintSupport

### Backends
* Poppler Qt (for PDF; _Required_)
* DjVULibre (for DjVu; _Optional_)
* Spectre (for ps; _Optional_)

### Optional
* Cups (for print support; _Optional_)

## Notes for compiling (Qt5 and Qt6) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/marcusbritanicus/qdocumentview.git qdocumentview`
- Enter the `qdocumentview` folder
  * `cd qdocumentview`
- Configure the project - we use meson for project management. Add `-Duse_qt_version=qt5` to the command below for the Qt5 version.
  * `meson setup .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`

## Sample Usage
```CPP
#include <QtWidgets>

#include <QDocumentView.hpp>
#include <QDocument.hpp>

int main( int argc, char *argv[] ) {
    QApplication app( argc, argv );

    /**
     * Add code to check the cli arguments.
     * It is assumed that
     */

    QDocumentView docView;

    /** You can use @doc to access it's properties, like title, author, etc */
    if ( argc == 2 ) {
        QDocument *doc = docView.load( argv[ 1 ] );
    }

    /**
     * Show the view as maximized.
     * Currently, we do not have a minimum size requirement */
    docView.showMaximized();

    return app.exec();
}
```

## Upcoming
* Plugins to support various document formats, like cbr, cbz, etc
* Any other feature you request for... :)

## Known Bugs
* Deleting QDocumentView pointer can potentially cause a SIGSEGV.
